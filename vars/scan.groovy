import com.hsbc.group.tooling.jenkins.scan.AppScan
import com.hsbc.group.tooling.jenkins.scan.IQScan


def call(Map config) {

    def exec_node = (config.exec_node) ? config.exec_node : 'cm-linux'
    if(config.scan_tool == 'iqscan') {
        stage('Nexus Lifecycle Analysis') {
            checkpoint()
            node(exec_node) {
                echo("Nexus Lifecycle Analysis Start")
                def iqscan = new IQScan()
                iqscan.init()

                // Makes call to build method of Maven.groovy object
                iqscan.performScan(config.iq_app, config.iq_org, config.iq_tag, config.iq_service_tier, config.iq_stage, config.scan_target)
                echo("Nexus Lifecycle Analysis Complete")
            }
        }
    } else if(config.scan_tool == 'appscan') {
        stage('AppScan Analysis') {
            checkpoint()
            node(exec_node) {
                echo("AppScan Start")
                def appscan = new AppScan()
                appscan.init()

                // Makes call to build method of Maven.groovy object
                appscan.performScan(config.project_api_key, config.project_id, config.scan_target, config.language, config.scan_config, config.time_out)
                echo("AppScan Complete")
            }
        }
    }
}

/*def call(Closure body) {
    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()
    def exec_node = (config.exec_node) ? config.exec_node : 'cm-linux'
    if(config.scan_tool == 'iqscan') {
        stage('Nexus Lifecycle Analysis') {
            checkpoint()
            node(exec_node) {
                echo("Nexus Lifecycle Analysis Start")
                def iqscan = new IQScan(this)
                // Makes call to build method of Maven.groovy object
                iqscan.performScan(config.iq_app, config.iq_org, config.iq_tag, config.iq_service_tier, config.iq_stage, config.scan_target)
                echo("Nexus Lifecycle Analysis Complete")
            }
        }
    } else if(config.scan_tool == 'appscan') {
        stage('AppScan Analysis') {
            checkpoint()
            node(exec_node) {
                echo("AppScan Start")
                def appscan = new AppScan(this)
                // Makes call to build method of Maven.groovy object
                appscan.performScan(config.project_api_key, config.project_id, config.scan_target, config.language, config.scan_config, config.time_out)
                echo("AppScan Complete")
            }
        }
    }
}*/