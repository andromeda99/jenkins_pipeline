import com.hsbc.group.tooling.jenkins.scm.Git
import com.hsbc.group.tooling.jenkins.build.Maven

def call(Map config) {

    def exec_node = (config.exec_node) ? config.exec_node : 'cm-linux'
    node(exec_node) {
        stage('Checkout Source') {
            echo("Checkout Stage Start")
            if(config.scm_tool == 'git') {
                def git = new Git()
                git.init()
                git.checkout repoUrl: "https://alm-github.systems.uk.hsbc/${config.scm_repo}.git", repoBranch: config.branch
            }
            echo("Checkout Stage Complete")
        }

        if(config.build_tool == 'mvn') {
            def cmd_args = (config.build_args) ? config.build_args : "clean package"
            def mvn_version = (config.mvn_version) ? config.mvn_version : (
                    (isUnix()) ? "3.3.3_Linux" : "3.3.3_Windows")
            def jdk_version = (config.jdk_version) ? config.jdk_version : ((isUnix()) ? "1.8.0.65_Linux" : "1.8.0.65_Windows")

            // Sets MAVEN_HOME and JAVA_HOME on Maven.groovy object.
	        def mvn = new Maven()
	        mvn.init(mvn_version, jdk_version)

            stage('Build') {
                echo("Build Stage Start")
                // Makes call to build method of Maven.groovy object
                mvn.build(cmd_args)
                echo("Build Stage Complete")
            }

            stage("Unit Tests Coverage") {
                echo("Unit Tests Coverage Stage Start")
                // Makes call to scan method of Maven.groovy object
                mvn.unitTestCoverage()
                echo("Unit Tests Coverage Stage Complete")
            }

            stage("Static Code Analysis") {
                echo("Static Code Analysis Stage Start")
                // Makes call to scan method of Maven.groovy object
                mvn.sonarScan()
                echo("Static Code Analysis Stage Complete")
            }

           stage("Publish Artefact to Nexus") {
                echo("Publish Artefact to Nexus Stage Start")
                // Makes call to scan method of Maven.groovy object
                mvn.publishToNexus()
                echo("Publish Artefact to Nexus Stage Complete")
            }
        } else if(config.tool == 'msbuild') {
            // TODO: code for MSBuild and Sonar scan
        } else {
            echo("No build tool selected.")
        }
    }
}