import com.hsbc.group.tooling.jenkins.deploy.Ansible

def call(Map config) {

    def exec_node = (config.exec_node) ? config.exec_node : 'cm-linux'
    node(exec_node) {
        stage('Deploy') {
            echo("Deploy Stage Start")
            if (config.deploy_tool == 'ansible') {
                def ansible = new Ansible()
                ansible.init(config.ansible_tower_url)

                if ('' != config.job_template_id
                    && null != config.job_template_id) {
                    ansible.launchJobTemplate(config.job_template_id)
                } else {
                    error("Ansible Tower job template Id (job_template_id) "
                        + "is a mandatory parameter required to trigger a "
                        + "deployment task via Ansible.")
                }
            } else {
                error("deploy_tool is a mandatory parameter required "
                    + "to trigger a deployment task.")
            }
        }
    }
}